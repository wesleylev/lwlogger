[version]: https://bitbucket.org/wesleylev/lwlogger/raw/14673424bbfac730ed161bc6011ef2dcce492911/build/libs/download.svg
[download]: https://bitbucket.org/wesleylev/lwlogger/downloads/LWLogger-1.0.0.jar
[license]: https://bitbucket.org/wesleylev/lwlogger/raw/14673424bbfac730ed161bc6011ef2dcce492911/build/libs/license.svg
[ ![version][] ][download]
[ ![license][] ](LICENSE.md)

#LWLogger 

LWLogger is an API including SLF4j.
It is a logger, having a configration...

## Built With

* [IntelliJ IDEA](https://www.jetbrains.com/idea/) - IDE
* [SLF4j](https://www.slf4j.org) - The SLF4j library

## Versioning

We use [SemVer](http://semver.org/) for versioning.

## Authors

* **Wesley Levasseur** - *Initial work* - [wesley_lev](https://gitlab.com/wesley_lev/)

See also the list of [contributors](https://bitbucket.org/wesleylev/lwlogger/) who participated in this project.

## License

This project is licensed under the Apache 2.0 License - see the [LICENSE.md](LICENSE.md) file for details

